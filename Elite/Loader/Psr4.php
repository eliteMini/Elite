<?php

namespace I7ssan\Loader;

/**
 * Autoload Elite Core File Using Psr4
 *
 * Class Psr4
 * @package Core\Loader
 */
class Psr4
{
    /**
     * @var array
     */
    private $prefixes = array();

    /**
     * @param string $prefix
     * @param string $baseDir
     *
     * @return void
     */
    public function add($prefix, $baseDir)
    {
        $prefix = trim($prefix, '\\').'\\';
        $baseDir = rtrim($baseDir, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR;
        $this->prefixes[] = array($prefix, $baseDir);
    }

    /**
     * @param string $class
     *
     * @return string|null
     */
    public function find($class)
    {
        $class = ltrim($class, '\\');

        foreach ($this->prefixes as $current)
        {
            list($currentPrefix, $currentBaseDir) = $current;
            if (0 === strpos($class, $currentPrefix))
            {
                $classWithoutPrefix = substr($class, strlen($currentPrefix));
                $file = $currentBaseDir.str_replace('\\', DIRECTORY_SEPARATOR, $classWithoutPrefix).'.php';
                if (file_exists($file))
                {
                    return $file;
                }
            }
        }
    }

    /**
     * @param string $class
     *
     * @return bool
     */
    public function load($class)
    {
        $file = $this->find($class);
        if (null !== $file)
        {
            require $file;

            return true;
        }

        return false;
    }

    /**
     * Registers this instance as an autoloader.
     *
     * @param bool $prepend
     */
    public function register($prepend = false)
    {
        spl_autoload_register(array($this, 'load'), true, $prepend);
    }

    /**
     * Removes this instance from the registered autoloaders.
     */
    public function unRegister()
    {
        spl_autoload_unregister(array($this, 'load'));
    }
}
